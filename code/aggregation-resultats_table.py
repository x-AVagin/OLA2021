
import argparse
import pathlib
import textwrap

import numpy as np
import pandas as pd

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Produce a table giving the number of BNs generated for all systems by all teh identification methods',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--bnpath',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                            Path where BNs are saved.
                            From this path, there is one folder per system in which there is one folder per identification methods
                            ''')
                        )
    parser.add_argument('--pathout',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path to where the csv table has to be saved
                        ''')
                        )

    args = parser.parse_args()
    # sys.argv = []
    # args = parser.parse_args(sys.argv)

    print(args)

    print("bim")

    table = dict()

    for systemid in args.bnpath.iterdir():
        if not systemid.is_dir():
            continue

        print('----------')
        print(systemid)
        table[systemid] = dict()

        # ----------
        # number of BNs:

        for identification_method in ["reveal", "bestfit"]:
            print(identification_method)

            path = systemid / identification_method / "info-BNs.txt"

            if path.exists():
                infoBN = pd.read_csv(path, sep='\t', header=0)
                table[systemid][
                    identification_method + "_nbBNs_generated"] = infoBN.nbBNs_generated[0]
                table[systemid][
                    identification_method + "_nbBNs_withInfluenceSignsOK"] = infoBN.nbBNs_withInfluenceSignsOK[0]
            else:
                table[systemid][
                    identification_method + "_nbBNs_generated"] = 0
                table[systemid][
                    identification_method + "_nbBNs_withInfluenceSignsOK"] = 0

        for identification_method in ["caspots", "our"]:
            print(identification_method)

            nb_bn = len(
                list((systemid / identification_method).glob("*.bnet")))
            table[systemid][identification_method] = nb_bn

        # ----------
        for identification_method in ["reveal", "bestfit", "caspots", "our"]:
            print(identification_method)

            # one datapoint of proportion of transition retrieved per bnet generated
            datapoint_coverage_proportion = []
            # systemid is the complete path of where the bn of the system are stored
            for coveragefile in (systemid / identification_method).glob(f"*_coverage_stgVSts_mixed.csv"):
                print(coveragefile)
                covtable = pd.read_csv(coveragefile, index_col=0)
                print(covtable)
                datapoint_coverage_proportion.append(
                    covtable["mixed"]["retreived_edges__proportion"])

            if len(datapoint_coverage_proportion) > 0:
                # coverage median (proportion)
                table[systemid][
                    identification_method + "_propcov_median"] = np.median(datapoint_coverage_proportion)
                # coverage mean (proportion)
                table[systemid][
                    identification_method + "_propcov_mean"] = np.mean(datapoint_coverage_proportion)
                # coverage std (proportion)
                table[systemid][
                    identification_method + "_propcov_std"] = np.std(datapoint_coverage_proportion)
                # coverage max (proportion)
                table[systemid][
                    identification_method + "_propcov_max"] = np.max(datapoint_coverage_proportion)
                # coverage min (proportion)
                table[systemid][
                    identification_method + "_propcov_min"] = np.std(datapoint_coverage_proportion)
            else:
                # coverage median (proportion)
                table[systemid][
                    identification_method + "_propcov_median"] = np.nan
                # coverage mean (proportion)
                table[systemid][
                    identification_method + "_propcov_mean"] = np.nan
                # coverage std (proportion)
                table[systemid][
                    identification_method + "_propcov_std"] = np.nan
                # coverage max (proportion)
                table[systemid][
                    identification_method + "_propcov_max"] = np.nan
                # coverage min (proportion)
                table[systemid][
                    identification_method + "_propcov_min"] = np.nan

    df = pd.DataFrame.from_dict(table, orient='index')

    df.to_csv(args.pathout)
    print("the end")
