
import argparse
import pathlib
import textwrap

import pandas as pd

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Produce a table giving the number of BNs generated for all systems by all teh identification methods',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--bnpath',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                            Path where BNs are saved.
                            From this path, there is one folder per system in which there is one folder per identification methods
                            ''')
                        )
    parser.add_argument('--pathout',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path to where the csv table has to be saved
                        ''')
                        )

    args = parser.parse_args()
    # sys.argv = []
    # args = parser.parse_args(sys.argv)

    print(args)

    print("bim")

    table = dict()

    for systemid in args.bnpath.iterdir():
        if not systemid.is_dir():
            continue

        print(systemid)
        table[systemid] = dict()

        for identification_method in ["reveal", "bestfit"]:
            print(identification_method)
            path = systemid / identification_method / "info-BNs.txt"

            if path.exists():
                infoBN = pd.read_csv(path, sep='\t', header=0)
                table[systemid][
                    identification_method + "_nbBNs_generated"] = infoBN.nbBNs_generated[0]
                table[systemid][
                    identification_method + "_nbBNs_withInfluenceSignsOK"] = infoBN.nbBNs_withInfluenceSignsOK[0]
            else:
                table[systemid][
                    identification_method + "_nbBNs_generated"] = 0
                table[systemid][
                    identification_method + "_nbBNs_withInfluenceSignsOK"] = 0

        for identification_method in ["caspots", "our"]:
            print(identification_method)

            nb_bn = len(
                list((systemid / identification_method).glob("*.bnet")))
            table[systemid][identification_method] = nb_bn

    df = pd.DataFrame.from_dict(table, orient='index')

    df.to_csv(args.pathout)
    print("the end")
