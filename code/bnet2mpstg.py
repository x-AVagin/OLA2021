import argparse
import textwrap

import mpbn

if __name__ == "__main__":

    # %% parse arguments
    parser = argparse.ArgumentParser(
        description='Description', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--bnetfile',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        bnet file containing the BN of which we want to find the STG
                        ''')
                        )
    parser.add_argument('--binarizedtscsv',
                        type=str,
                        required=False,
                        help=textwrap.dedent('''\
                        Optinal. csv of the binaried ts. If given, a path about the configurations is added in the sif. The edge type is "obs"
                        ''')
                        )

    mbn = mpbn.MPBooleanNetwork({
        "a": "!b",
        "b": "!a",
        "c": "!a & b"})
    list(mbn.attractors())
    [{'a': 0, 'b': 1, 'c': 1}, {'a': 1, 'b': 0, 'c': 0}]
    mbn.reachability({'a': 0, 'b': 1, 'c': 1}, {'a': 1, 'b': 0, 'c': 0})

    mbn.reachability({'a': 0, 'b': 0, 'c': 0}, {'a': 1, 'b': 1, 'c': 1})

    list(mbn.attractors(reachable_from={'a': 0, 'b': 1, 'c': 0}))

    mbn = mpbn.MPBooleanNetwork({
        "a": "!b",
        "b": "!a",
        "c": "(!a & b) | (a & !b) "})
    list(mbn.attractors())
    [{'a': 0, 'b': 1, 'c': 1}, {'a': 1, 'b': 0, 'c': 0}]
    mbn.reachability({'a': 0, 'b': 1, 'c': 1}, {'a': 1, 'b': 0, 'c': 0})

    mbn.reachability({'a': 0, 'b': 0, 'c': 0}, {'a': 1, 'b': 1, 'c': 1})

    list(mbn.attractors(reachable_from={'a': 0, 'b': 1, 'c': 0}))
