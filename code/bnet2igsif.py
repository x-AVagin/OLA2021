import argparse
import sys
import textwrap

import PyBoolNet

# %pwd

try:
    sys.path.insert(1, 'code/')
    import pknhandler
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


if __name__ == "__main__":

    # %% help message
    parser = argparse.ArgumentParser(
        description="""
        Extracted the interaction graph of a bnet file
        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    # script name, res-simu.txt pknlpoutfilepath, plot option, optimization option, dirpath where to store frequency of values from the streatched ts
    parser.add_argument('--bnetfilepath',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        Bnet file to read
                        ''')
                        )
    parser.add_argument('--sifoutfilepath',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        Sif file in hich store the interaction graph
                        ''')
                        )

    # %% Retrieve arguments
    args = parser.parse_args()

    #args.bnetfilepath = "/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/data/PyBoolNet-data/raw/arellano_rootstem/arellano_rootstem.bnet"
    #args.sifoutfilepath = "/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/data/PyBoolNet-data/processed/arellano_rootstem/arellano_rootstem.sif"

    primes = PyBoolNet.FileExchange.bnet2primes(args.bnetfilepath)

    igraph = PyBoolNet.InteractionGraphs.primes2igraph(primes)

    interactions = set()

    for (nodein, nodeout) in igraph.edges():
        sign_info = igraph[nodein][nodeout]["sign"]
        for sign in sign_info:
            # like we do when extracted the ts,
            # add te prefix "node_" to sanitize the node names
            # (because ASP needs nodes starting with a small letter)
            interactions.add(("node_" + nodein, sign, "node_" + nodeout))

    pknhandler.sifstruct2siffile(interactions, args.sifoutfilepath)
