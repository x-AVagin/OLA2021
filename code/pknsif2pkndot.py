
import argparse
import logging
import sys
import textwrap

try:
    sys.path.insert(1, 'code/')
    import pknhandler
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


logger = logging.getLogger('logger.pknsif2pkndot')


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='From pknsif to pkndot', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--siffilepath',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        Path of the sif file containing the PKN.
                        ''')
                        )
    parser.add_argument('--dotfilepath',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        Path of the dot file we will write the PKN in.
                        ''')
                        )

    # sys.argv = [ "--siffilepath", "data/RBoolNet_yeast/pkn/yeast-withknowledge.sif", "--dotfilepath", "test_sif2dot.dot"]
    # args = parser.parse_args(sys.argv)
    args = parser.parse_args()

    logger.info(
        f"""
        Given sif is {args.siffilepath}.
        The dot file in which to write is {args.dotfilepath}.
        """)

    pknhandler.siffile2dotfile(args.siffilepath, args.dotfilepath)
    logger.info(f"dot file has been written in {args.dotfilepath}")
